package com.example.animal.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.animal.Model.Dog;

public class DogService {
    public List<Dog> createDogs() {
        ArrayList<Dog> DogList = new ArrayList<>();
        Dog Dog1 = new Dog("Ben1");
        Dog Dog2 = new Dog("Ben2");
        Dog Dog3 = new Dog("Ben3");
        DogList.addAll(Arrays.asList(Dog1, Dog2, Dog3));

        return DogList;
    }
}

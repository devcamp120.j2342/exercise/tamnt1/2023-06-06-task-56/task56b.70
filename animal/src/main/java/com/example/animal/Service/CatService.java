package com.example.animal.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.animal.Model.Cat;

public class CatService {
    public List<Cat> createCats() {
        ArrayList<Cat> catList = new ArrayList<>();
        Cat cat1 = new Cat("Fluffy1");
        Cat cat2 = new Cat("Fluffy2");
        Cat cat3 = new Cat("Fluffy3");
        catList.addAll(Arrays.asList(cat1, cat2, cat3));

        return catList;
    }
}

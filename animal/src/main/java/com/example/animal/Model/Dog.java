﻿package com.example.animal.Model;

public class Dog extends Mammal {

    public Dog(String name) {
        super(name);
    }

    public void greet() {
        System.out.println("Woof");
    }

    public void greets(Dog anotherDog) {
        System.out.println("Woooooof");
    }

    @Override
    public String toString() {
        return "Dog[Mammal[Animal[name=" + super.toString() + "]";
    }

}

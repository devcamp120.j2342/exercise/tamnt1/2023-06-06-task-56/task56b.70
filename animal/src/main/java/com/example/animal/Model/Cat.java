﻿package com.example.animal.Model;

public class Cat extends Mammal {

    public Cat(String name) {
        super(name);
        // TODO Auto-generated constructor stub
    }

    public void greet() {
        System.out.println("Meow");
    }

    @Override
    public String toString() {
        return "Cat[Mammal[Animal[name=" + super.toString() + "]";
    }

}

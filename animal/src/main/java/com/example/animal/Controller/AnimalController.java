package com.example.animal.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.animal.Model.Cat;
import com.example.animal.Model.Dog;
import com.example.animal.Service.CatService;
import com.example.animal.Service.DogService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AnimalController {
    @Autowired
    private CatService catService;
    private DogService dogService;

    @GetMapping("/cats")
    public List<Cat> getCats() {
        return catService.createCats();
    }

    @GetMapping("/dogs")
    public List<Dog> getDogs() {
        return dogService.createDogs();
    }
}
